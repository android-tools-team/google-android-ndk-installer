# German translation of google-android-ndk-installer.
# This file is distributed under the same license as the
#     google-android-ndk-installer package.
# Copyright (C) of this file Chris Leick <c.leick@vollbio.de>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: google-android-ndk-installer\n"
"POT-Creation-Date: 2016-07-19 20:36+0000\n"
"PO-Revision-Date: 2016-08-05 13:20+0100\n"
"Last-Translator: Chris Leick <c.leick@vollbio.de>\n"
"Language-Team: Debian German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. Type: select
#. Description
msgid "Mirror to download packages?"
msgstr "Von welchem Spiegelserver sollen die Pakete heruntergeladen werden?"

#. Type: select
#. Description
#: ../templates:1001
msgid ""
"Please select your preferred mirror to download Google's Android packages "
"from."
msgstr ""
"Bitte wählen Sie Ihren bevorzugten Spiegelserver aus, von dem Googles "
"Android-Pakete heruntergeladen werden sollen."
